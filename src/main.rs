mod byte_packet_buffer;
mod dns_header;
mod dns_packet;
mod dns_question;
mod dns_record;
mod result_code;
mod query_type;

use byte_packet_buffer::BytePacketBuffer;
use dns_packet::DnsPacket;
use dns_question::DnsQuestion;
use dns_record::DnsRecord;
use query_type::QueryType;
use result_code::ResultCode;

use std::net::{
    Ipv4Addr,
    SocketAddr,
    UdpSocket,
};
use std::io::{
    Error,
    ErrorKind,
    Result,
};

fn get_question(socket: &UdpSocket, req_packet: &mut DnsPacket) -> Result<SocketAddr> {
    let mut req_buffer = BytePacketBuffer::new();
    let (_, src) = match socket.recv_from(&mut req_buffer.buf) {
        Ok(x) => x,
        Err(e) => {
            println!("Failed to read from UDP socket: {:?}", e);
            return Err(e);
        },
    };
    match req_packet.set_from_buffer(&mut req_buffer) {
        Ok(_) => {},
        Err(e) => {
            println!("Failed to parse UDP query packet: {:?}", e);
            return Err(e);
        },
    };

    Ok(src)
}

fn send_response(socket: &UdpSocket, src: SocketAddr, res_packet: &mut DnsPacket) {
    let mut res_buffer = BytePacketBuffer::new();
    match res_packet.write(&mut res_buffer) {
        Ok(_) => {},
        Err(e) => {
            println!("Failed to encode UDP response packet: {:?}", e);
            return;
        }
    };

    let len = res_buffer.pos();
    let data = match res_buffer.get_range(0, len) {
        Ok(x) => x,
        Err(e) => {
            println!("Failed to retrieve response buffer: {:?}", e);
            return;
        }
    };

    match socket.send_to(data, src) {
        Ok(_) => {},
        Err(e) => {
            println!("Failed to send response buffer: {:?}", e);
        },
    };
}

fn lookup(qname: &str, qtype: QueryType, server: (&str, u16)) -> Result<DnsPacket> {
    let socket = UdpSocket::bind(("0.0.0.0", 43210))?;

    let mut packet = DnsPacket::new();

    packet.header.id = 6666;
    packet.header.questions = 1;
    packet.header.recursion_desired = true;
    packet.questions.push(DnsQuestion::new(qname.to_string(), qtype));

    let mut req_buffer = BytePacketBuffer::new();
    packet.write(&mut req_buffer).unwrap();
    match socket.send_to(&req_buffer.buf[0..req_buffer.pos], server) {
        Ok(_) => {},
        Err(e) => {
            println!("Fail to send packet to DNS server: {:?}", e);
            return Err(e);
        },
    };

    let mut res_buffer = BytePacketBuffer::new();
    match socket.recv_from(&mut res_buffer.buf) {
        Ok(_) => {},
        Err(e) => {
            println!("Fail to received packet from DNS server: {:?}", e);
            return Err(e);
        },
    };

    DnsPacket::from_buffer(&mut res_buffer)
}

fn try_resolved(qname: &str,
                qtype: QueryType,
                vec_ns: &Vec<(Ipv4Addr, String)>,
                new_ns: &mut Vec<(Ipv4Addr, String)>) -> Result<Option<DnsPacket>> {
    for ns in vec_ns {
        let (ip, name) = (&ns.0.to_string(), &ns.1);
        println!("Attempting lookup of {:?} {} with ns {}({})", qtype, qname, ip, name);
        let server = (ip.as_str(), 53);
        let response = match lookup(qname, qtype.clone(), server) {
            Ok(x) => x,
            Err(e) => return Err(e),
        };

        // If (answer exist and  no error) or domain does not exist, return
        // response
        if (!response.answers.is_empty() && response.header.rescode == ResultCode::NOERROR)
            || response.header.rescode == ResultCode::NXDOMAIN {
            return Ok(Some(response.clone()));
        }

        // If find a new server, retry the loop;
        let mut resolved = response.get_resolved(qname);
        if resolved.is_empty() {
            continue;
        } else {
            new_ns.append(&mut resolved);
            break;
        }
    }

    Ok(None)
}

fn try_unresolved(qname: &str,
                  qtype: QueryType,
                  vec_ns: &Vec<(Ipv4Addr, String)>,
                  new_ns: &mut Vec<(Ipv4Addr, String)>) -> Result<()> {

    // Loop over known servers
    for ns in vec_ns {
        // Get the server names
        let (ip, _) = (&ns.0.to_string(), &ns.1);
        let server = (ip.as_str(), 53);
        let response = lookup(qname, qtype.clone(), server)?;
        let ns_name = response.get_unresolved(qname);

        // Loop
        for host in ns_name {
            // Find the addr of the new server name
            let mut resolved = match recursive_lookup(&host, QueryType::A) {
                Ok(x) => x.get_resolved(qname),
                Err(e) => return Err(e),
            };

            // If server ip are found
            if !resolved.is_empty() {
                new_ns.append(&mut resolved);
                return Ok(())
            }
        }
    }

    // If nothing found, it's a dead end
    println!("All paths have been used without results.");
    Err(Error::new(ErrorKind::InvalidInput, "No more path available"))
}

fn recursive_lookup(qname: &str, qtype: QueryType) -> Result<DnsPacket> {
    // Use root server as first
    let mut vec_ns: Vec<(Ipv4Addr, String)> = Vec::new();
    vec_ns.push((Ipv4Addr::new(198, 41, 0, 4), "a.root".to_string()));

    loop {
        let mut new_ns: Vec<(Ipv4Addr, String)> = Vec::new();
        // Try to get the response OR get the next servers from DnsRecord::A.
        match try_resolved(qname, qtype, &vec_ns, &mut new_ns) {
            Ok(None) => {},
            Ok(Some(x)) => return Ok(x),
            Err(e) => return Err(e),
        };

        // If nothing found, try to get next server from DnsRecord::NS
        if new_ns.is_empty() {
            match try_unresolved(qname, qtype, &vec_ns, &mut new_ns) {
                Ok(_) => {},
                Err(e) => return Err(e),
            };
        }

        // Loop on new servers found
        vec_ns.clear();
        vec_ns.append(&mut new_ns);
    }
}

fn main() {

    let socket = UdpSocket::bind(("0.0.0.0", 2053)).unwrap();

    loop {
        // Listen to request
        let mut req_packet = DnsPacket::new();
        let src = match get_question(&socket, &mut req_packet) {
            Ok(x) => x,
            Err(_) => continue,
        };

        // Build response
        let mut res_packet = DnsPacket::new();
        res_packet.header.id = req_packet.header.id;
        res_packet.header.recursion_desired = true;
        res_packet.header.recursion_available = true;
        res_packet.header.response = true;

        if req_packet.questions.is_empty() {
            res_packet.header.rescode = ResultCode::FORMERR;
        } else {
            let question = &req_packet.questions[0];
            println!("Receied query: {:?}", question);

            if let Ok(result) = recursive_lookup(&question.name, question.qtype) {
                res_packet.questions.push(question.clone());
                res_packet.header.rescode = result.header.rescode;

                for rec in result.answers {
                    println!("Answer: {:?}", rec);
                    res_packet.answers.push(rec);
                }
                for rec in result.authorities {
                    println!("Authority: {:?}", rec);
                    res_packet.authorities.push(rec);
                }
                for rec in result.resources {
                    println!("Resource: {:?}", rec);
                    res_packet.resources.push(rec);
                }
            } else {
                res_packet.header.rescode = ResultCode::SERVFAIL;
            }

            send_response(&socket, src, &mut res_packet);
        }
    }
}
