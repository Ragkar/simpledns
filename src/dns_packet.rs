use crate::byte_packet_buffer::BytePacketBuffer;
use crate::dns_header::DnsHeader;
use crate::dns_question::DnsQuestion;
use crate::dns_record::DnsRecord;
use crate::query_type::QueryType;

use std::io::Result;
use std::net::Ipv4Addr;

#[derive(Clone, Debug)]
pub struct DnsPacket {
    pub header: DnsHeader,
    pub questions: Vec<DnsQuestion>,
    pub answers: Vec<DnsRecord>,
    pub authorities: Vec<DnsRecord>,
    pub resources: Vec<DnsRecord>,
}

impl DnsPacket {

    pub fn new() -> DnsPacket {
        DnsPacket {
            header: DnsHeader::new(),
            questions: Vec::new(),
            answers: Vec::new(),
            authorities: Vec::new(),
            resources: Vec::new(),
        }
    }

    pub fn from_buffer(buffer: &mut BytePacketBuffer) -> Result<DnsPacket> {
        let mut result = DnsPacket::new();
        result.set_from_buffer(buffer)?;
        Ok(result)
    }

    pub fn set_from_buffer(&mut self, buffer: &mut BytePacketBuffer) -> Result<()> {
        self.header.read(buffer)?;

        for _ in 0..self.header.questions {
            let mut question = DnsQuestion::new("".to_string(), QueryType::UNKNOWN(0));
            question.read(buffer)?;
            self.questions.push(question);
        }

        for _ in 0..self.header.answers {
            let rec = DnsRecord::read(buffer)?;
            self.answers.push(rec);
        }

        for _ in 0..self.header.authoritave_entries {
            let rec = DnsRecord::read(buffer)?;
            self.authorities.push(rec);
        }

        for _ in 0..self.header.resource_entries {
            let rec = DnsRecord::read(buffer)?;
            self.resources.push(rec);
        }

        Ok(())
    }

    pub fn write(&mut self, buffer: &mut BytePacketBuffer) -> Result<()> {
        self.header.questions = self.questions.len() as u16;
        self.header.answers = self.answers.len() as u16;
        self.header.authoritave_entries = self.authorities.len() as u16;
        self.header.resource_entries = self.resources.len() as u16;

        self.header.write(buffer)?;
        for question in &self.questions {
            question.write(buffer)?;
        }
        for rec in &self.answers {
            rec.write(buffer)?;
        }
        for rec in &self.authorities {
            rec.write(buffer)?;
        }
        for rec in &self.resources {
            rec.write(buffer)?;
        }

        Ok(())
    }

    // Return addr from NS
    pub fn get_resolved(&self, qname: &str) -> Vec<(Ipv4Addr, String)> {
        let mut new_authorities = Vec::new();
        for auth in &self.authorities {
            // Search NS record ending with the qname searched
            if let DnsRecord::NS { ref domain, ref host, .. } = *auth {
                if !qname.ends_with(domain) {
                    continue;
                }

                // Search the corresponding A record.
                for rsrc in &self.resources {
                    if let DnsRecord::A { ref domain, ref addr, .. } = *rsrc {
                        if domain != host {
                            continue;
                        }

                        new_authorities.push((addr.clone(), domain.clone()));
                    }
                }
            }
        }

        new_authorities
    }

    // Return name from NS
    pub fn get_unresolved(&self, qname: &str) -> Vec<String> {
        let mut new_authorities = Vec::new();
        for auth in &self.authorities {
            if let DnsRecord::NS { ref domain, ref host, .. } = *auth {
                if !qname.ends_with(domain) {
                    continue;
                }

                new_authorities.push(host.clone());
            }
        }

        return new_authorities
    }

}
