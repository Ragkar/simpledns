use std::io::{
    Error,
    ErrorKind,
    Result,
};

const SIZE: usize = 1024;

pub struct BytePacketBuffer {
    pub buf: [u8; SIZE],
    pub pos: usize,
}

impl BytePacketBuffer {

    pub fn new() -> BytePacketBuffer {
        BytePacketBuffer {
            buf: [0; SIZE],
            pos: 0,
        }
    }

    /*-- Functions to handle pos --*/

    pub fn pos(&self) ->usize {
        self.pos
    }

    pub fn step(&mut self, steps: usize) -> Result<()> {
        self.pos += steps;
        Ok(())
    }

    fn seek(&mut self, pos: usize) -> Result<()> {
        self.pos = pos;
        Ok(())
    }

    /*-- Functions to read --*/

    // Read a update pos
    fn read_u8(&mut self) -> Result<u8> {
        if self.pos >= SIZE {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }
        let res = self.buf[self.pos];
        self.pos += 1;
        Ok(res)
    }

    // Read and does not update pos;
    fn get(&mut self, pos: usize) -> Result<u8> {
        if pos >= SIZE {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }
        Ok(self.buf[pos])
    }

    pub fn get_range(&mut self, start: usize, len: usize) -> Result<&[u8]> {
        if start + len >= SIZE {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }
        Ok(&self.buf[start..start+len as usize])
    }

    pub fn read_u16(&mut self) -> Result<u16> {
        let res = ((self.read_u8()? as u16) << 8)
                | (self.read_u8()? as u16);
        Ok(res)
    }

    pub fn read_u32(&mut self) -> Result<u32> {
        let res = ((self.read_u16()? as u32) << 16)
                | (self.read_u16()? as u32);
        Ok(res)
    }

    pub fn read_qname(&mut self, outstr: &mut String) -> Result<()> {
        // Info in case we jump
        let mut pos = self.pos();
        let mut jumped = false;

        let mut delim = "";
        loop {
            // Read the len of the label
            let len = self.get(pos)?;

            // Is it a jump ?
            if (len & 0xC0) == 0xC0 {
                // Update buffer position to a point pas the current label.
                if !jumped {
                    self.seek(pos + 2)?
                }

                // Compute the offset
                let b2 = self.get(pos + 1)? as u16;
                let offset = (((len as u16) ^ 0xC0) << 8) | b2;
                // Jump !
                pos = offset as usize;
                jumped = true;
            } else {
                // Skip len byte
                pos += 1;

                // End of domain names ?
                if len == 0 {
                    break;
                }

                // Push delim, then the label
                outstr.push_str(delim);
                let str_buffer = self.get_range(pos, len as usize)?;
                outstr.push_str(&String::from_utf8_lossy(str_buffer).to_lowercase());

                // Update delim and pos
                delim = ".";
                pos += len as usize;
            }
        }

        // If it hasn't jump, update the buffer position
        if !jumped {
            self.seek(pos)?;
        }

        Ok(())
    }

    /*-- Functions to write --*/

    pub fn write_u8(&mut self, val: u8) -> Result<()> {
        if self.pos >= SIZE {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }
        self.buf[self.pos] = val;
        self.pos += 1;
        Ok(())
    }

    pub fn write_u16(&mut self, val: u16) -> Result<()> {
        self.write_u8((val >> 8) as u8)?;
        self.write_u8((val & 0xFF) as u8)?;
        Ok(())
    }

    pub fn write_u32(&mut self, val: u32) -> Result<()> {
        self.write_u16((val >> 16) as u16)?;
        self.write_u16((val & 0xFFFF) as u16)?;
        Ok(())
    }

    pub fn write_qname(&mut self, qname: &str) -> Result<()> {
        let split_str = qname.split('.').collect::<Vec<&str>>();
        for label in split_str {

            let len = label.len();
            if len > 0x34 {
                return Err(Error::new(ErrorKind::InvalidInput, "Single label exceeds 63 characters."));
            }

            self.write_u8(len as u8)?;
            for b in label.as_bytes() {
                self.write_u8(*b)?;
            }
        }

        self.write_u8(0)?;
        Ok(())
    }

    pub fn set_u8(&mut self, npos: usize, val: u8) -> Result<()> {
        self.buf[npos] = val;
        Ok (())
    }

    pub fn set_u16(&mut self, npos: usize, val: u16) -> Result<()> {
        self.set_u8(npos, (val >> 8) as u8)?;
        self.set_u8(npos + 1, (val & 0xFF) as u8)?;
        Ok (())
    }
}
